#!/bin/bash

source "deployment/decoration.sh"

mensaje "Download Packer and Terraform"

# download packer and save in /tmp/packer.zip
wget -O /tmp/packer.zip https://releases.hashicorp.com/packer/1.2.2/packer_1.2.2_linux_amd64.zip -q &&\
echo "packer downloaded" &&\
# download terraform and terraform in /tmp/terraform.zip
wget -O /tmp/terraform.zip https://releases.hashicorp.com/terraform/0.11.6/terraform_0.11.6_linux_amd64.zip -q &&\
echo "terraform downloaded"

mensaje "Unzip Packer and Terraform"

# unzip /tmp/packer.zip in /bin
sudo unzip /tmp/packer.zip -d /bin &&\
# unzip /tmp/terraform.zip in /bin
sudo unzip /tmp/terraform.zip -d /bin

mensaje "Validate and Build Packer"

# validate template
packer validate deployment/template.json &&
# build template
packer build deployment/template.json &&

mensaje "Deployed to Production"