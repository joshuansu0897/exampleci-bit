const assert = require('assert');
const cal = require('../src/Utils/Calculator')

describe('Calculator Test', async () => {
  describe('#sum(n1,n2)', async () => {

    it('correct way', async () => {
      assert.equal(cal.sum(5, 5), 10);
    });

    it('n1 zero', async () => {
      assert.equal(cal.sum(0, 5), 5);
    });

    it('n2 zero', async () => {
      assert.equal(cal.sum(5, 0), 5);
    });

    it('n1 undefined retuns 0', async () => {
      assert.equal(cal.sum(undefined, 5), 0);
    });

    it('n2 undefined retuns 0', async () => {
      assert.equal(cal.sum(5, undefined), 0);
    });

    it('one number', async () => {
      assert.equal(cal.sum(5), 0);
    });

  });

  describe('#subtract(n1,n2)', async () => {

    it('correct way', async () => {
      assert.equal(cal.subtract(5, 5), 0);
    });

    it('n1 zero', async () => {
      assert.equal(cal.subtract(0, 5), -5);
    });

    it('n2 zero', async () => {
      assert.equal(cal.subtract(5, 0), 5);
    });

    it('n1 undefined retuns 0', async () => {
      assert.equal(cal.subtract(undefined, 5), 0);
    });

    it('n2 undefined retuns 0', async () => {
      assert.equal(cal.subtract(5, undefined), 0);
    });

    it('one number', async () => {
      assert.equal(cal.subtract(5), 0);
    });

  });

  describe('#multiplication(n1,n2)', async () => {

    it('correct way', async () => {
      assert.equal(cal.multiplication(5, 5), 25);
    });

    it('n1 zero', async () => {
      assert.equal(cal.multiplication(0, 5), 0);
    });

    it('n2 zero', async () => {
      assert.equal(cal.multiplication(5, 0), 0);
    });

    it('n1 undefined retuns 0', async () => {
      assert.equal(cal.multiplication(undefined, 5), 0);
    });

    it('n2 undefined retuns 0', async () => {
      assert.equal(cal.multiplication(5, undefined), 0);
    });

    it('one number', async () => {
      assert.equal(cal.multiplication(5), 0);
    });

  });

  describe('#divide(n1,n2)', async () => {

    it('correct way', async () => {
      assert.equal(cal.divide(5, 5), 1);
    });

    it('n1 zero', async () => {
      assert.equal(cal.divide(0, 5), 0);
    });

    it('n2 zero', async () => {
      assert.equal(cal.divide(5, 0), 0);
    });

    it('n1 undefined retuns 0', async () => {
      assert.equal(cal.divide(undefined, 5), 0);
    });

    it('n2 undefined retuns 0', async () => {
      assert.equal(cal.divide(5, undefined), 0);
    });

    it('one number', async () => {
      assert.equal(cal.divide(5), 0);
    });

  });
});