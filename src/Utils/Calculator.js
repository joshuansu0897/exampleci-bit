const Raven = require('raven');
Raven.config('https://fc5deb8879174f5ca91ddfb303b3f685@sentry.io/1223776').install();

/**
 * Sum n1 and n2
 * @param {Number} n1 
 * @param {Number} n2 
 * @returns {Number} result of the operation
 */
exports.sum = (n1, n2) => {
  if (isNaN(n1) || isNaN(n2)) {
    try {
      throw new NotNumberException();
    } catch (e) {
      Raven.captureException(e);
    }
    return 0
  }
  return n1 + n2
}

/**
 * Subtract n2 to n1
 * @param {Number} n1 
 * @param {Number} n2 
 * @returns {Number} result of the operation
 */
exports.subtract = (n1, n2) => {
  if (isNaN(n1) || isNaN(n2)) {
    try {
      throw new NotNumberException();
    } catch (e) {
      Raven.captureException(e);
    }
    return 0
  }
  return n1 - n2
}

/**
 * multiply n1 with n2
 * @param {Number} n1 
 * @param {Number} n2 
 * @returns {Number} result of the operation
 */
exports.multiplication = (n1, n2) => {
  if (isNaN(n1) || isNaN(n2)) {
    try {
      throw new NotNumberException();
    } catch (e) {
      Raven.captureException(e);
    }
    return 0
  }
  return n1 * n2
}

/**
 * Divide n1 in n2
 * @param {Number} n1
 * @param {Number} n2 
 * @returns {Number} result of the operation
 */
exports.divide = (n1, n2) => {
  if (isNaN(n1) || isNaN(n2) || n2 == 0) {
    try {
      throw new NotNumberException();
    } catch (e) {
      Raven.captureException(e);
    }
    return 0
  }
  return n1 / n2
}